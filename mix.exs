defmodule UinewsBot.Mixfile do
  use Mix.Project

  def project do
    [
      app: :uinews_bot,
      version: "0.1.0",
      elixir: "~> 1.10",
      default_task: "server",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  def application do
    [
      applications: [:logger, :nadia],
      mod: {UinewsBot, []}
    ]
  end

  defp deps do
    [
      {:nadia, "~> 0.7.0"},
      {:poison, "~> 4.0"}
    ]
  end

  defp aliases do
    [
      server: "run --no-halt"
    ]
  end
end
