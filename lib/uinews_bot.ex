defmodule UinewsBot do
  use Application

  def start(_type, _args) do
    bot_name = Application.get_env(:uinews_bot, :bot_name)

    unless String.valid?(bot_name) do
      IO.warn("""
      Env not found Application.get_env(:uinews_bot, :bot_name)
      This will give issues when generating commands
      """)
    end

    if bot_name == "" do
      IO.warn("An empty bot_name env will make '/anycommand@' valid")
    end

    import Supervisor.Spec, warn: false

    children = [
      worker(UinewsBot.Poller, []),
      worker(UinewsBot.Matcher, [])
    ]

    opts = [strategy: :one_for_one, name: UinewsBot.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
